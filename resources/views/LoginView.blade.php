<html>
<head>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    
    <div class="container" align ="center">
        <div class="alert alert-danger" role="alert">
            <font face="微軟正黑體">
                <h1 class="display-4"> 登入 MessageBoard</h1>
            </font>
        </div>   
    </div>   

</head>
<body style="background-color:#FFF0F5;">


<form method="post" action="/">

<div class="container">

    <div class="form-group sm-2" align ="center">
        <label for="exampleFormControlInput1" class="col-sm-0 col-form-label">
            <span class="badge badge-primary">
                <font face="微軟正黑體">   
                    <h4>帳號</h4>
                </font>
            </span>
        </label>
        <input  class="form-group mx-sm-9 mb-3" name="account" placeholder="請輸入帳號" 
        onkeyup="value=value.replace(/[\W]/g,'') " 
        onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" required>
    </div>
    
    <div class="form-group" align ="center">
        <label for="exampleFormControlInput1" class="col-sm-0 col-form-label">
            <span class="badge badge-success">
                <font face="微軟正黑體">
                    <h4>密碼</h4>
                </font>
            </span>
        </label>
        <input type="password" class="form-group mx-sm-9 mb-2" name="password" placeholder="請輸入密碼" 
        onkeyup="value=value.replace(/[\W]/g,'') " 
        onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" required>
    </div>
    
    <div align ="center">
    <input type="submit" class="btn btn-outline-info"  name="sign" value="登入"/>
        <br/>
        <br/>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>

<form action="register/views">
        <font face="微軟正黑體">   
            <h5>
                還沒有帳號嗎?
            </h5>
        </font>
        <input type ="submit" class="btn btn-outline-info" value="立即註冊">
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form><hr/>

</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
@if (!empty( $login_message ))
    <input type="hidden" id="message" value =" {{ $login_message }} ">
    <script>
        var msg = $("#message").val();
        alert(msg);
    </script>
@endif
</body>
</html>
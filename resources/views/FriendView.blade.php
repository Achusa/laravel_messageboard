<meta name="csrf_token" content="{{ csrf_token() }}" />
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <div class="container" align ="center">
        <div class="alert alert-danger" role="alert">
            <font face="微軟正黑體">
                <h1 class="display-4">{{ $user['username'] }}的好友名單</h1>
            </font>
        </div>
    </div>

</head>

<body style="background-color:#FFF0F5;padding:10px">

<form action="/">
    <button type="submit"  class="btn btn-outline-primary">回留言板🏠</button>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>

<form method="post" action="/">
    <button type="button" id="logout" class="btn btn-outline-danger" onclick="deleteCookie()">登出</button>
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>

<div class="alert alert-warning" role="alert">
    <font face="微軟正黑體">
        <h2 class="display-10">{{ $user['username'] }}的好友名單</h2>
    </font>
</div>
<hr/>
<input type="hidden" id="user" value =" {{ $user['account'] }} ">
@if (!empty($invitation))
@foreach ($invitation as $value)

<form id="invitationTitle">
    <div class="alert alert-danger" role="alert">{{ $user['username'] }}好友邀請</div>
</form>

<form id="invitation" method = "post" action = "/friend">
    <tr>
        <td>
            <font face="微軟正黑體">{{ $value['applicant_account'] }}</font>
        </td>
        <td>
            <button type="button" class="btn btn-outline-success" value="{{ $value['applicant_account'] }}" onclick="acceptInvitation(this)">✔接受</button>
            <button type="button" class="btn btn-outline-danger" value="{{ $value['applicant_account'] }}" onclick="refuseInvitation(this)">✘拒絕</button>
        </td>
    </tr>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <hr/>
</form>
@endforeach
@endif

<form method = "post" action = "/friend">
    <input type = "text" class= "btn" name= "user_account" placeholder= "🔍輸入想搜尋的帳號"  onkeyup="value=value.replace(/[\W]/g,'') "
        onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" required>
    <button type="button"  class="btn btn-secondary" onclick="this.disabled=true;this.form.submit()">搜尋好友</button>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <br>
</form>

<form id="friendInvitation">
    @if (!empty($search_result_msg))
        @if ($search_result)
            <font face="微軟正黑體">{{ $search_result_msg['username'] }}({{ $search_result_msg['account'] }})</font>
            <button type="button" class="btn btn-outline-primary" onclick="friendInvitation()">送出好友邀請</button>
            <input id="friend_account" type="hidden" value="{{ $search_result_msg['account'] }}">
            <input id="user_account" type="hidden" value="{{ $user['account'] }}">
            <hr/>
        @else
            @if (is_array($search_result_msg))
            <font face="微軟正黑體" style="padding:10px">🔥{{ $search_result_msg['username'] }}({{ $search_result_msg['account'] }})和你已經是朋友囉!🔥</font>
            @else
            <font face="微軟正黑體" style="padding:10px">🔥🔥🔥{{ $search_result_msg }}🔥🔥🔥</font>
            @endif
        @endif
    @endif
</form>

@if (!empty($friend_data))
@foreach ($friend_data as $value)
<form id="{{ $value['friend_account'] }}">
    <table>
    <tr>
        <th><h3 class="display-10" style="padding:10px">{{ $value['friend_account'] }}</h3></th>
        <th><button class="btn btn-outline-danger" value="{{ $value['friend_account'] }}" onclick="deleteFriend(this)">刪除好友</button></th>
    </tr>
    </table>
</form>
@endforeach
@endif

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

</body>
</html>

<script>
function deleteCookie()
{
    if (confirm('確定要登出嗎?')) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
        $.ajax({
            url:"/",
            type:"DELETE",
            success: function () {
                alert('登出成功');
                (location.href='/');
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        })
    }
}

function friendInvitation()
{
    if (confirm('確定要送出嗎?')) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
        $.ajax({
            url:"/invitation",
            type:"PUT",
            data:{
                applicant_account:$("#user_account").val(),
                friend_account:$("#friend_account").val(),
            },
            success: function (response) {
                alert(response);
                (location.href='/friend');
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        })
    }
}

function acceptInvitation(applicant_account)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
        }
    });
    $.ajax({
        url:"/friend",
        type:"PUT",
        data:{
            f_account:$("#user").val(),
            friend_account:applicant_account.value,
        },
        success: function (response) {
            alert(response);
            (location.href='/friend');
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    })
}

function refuseInvitation(applicant_account)
{
    if (confirm('確定要拒絕嗎?')) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
        $.ajax({
            url:"/invitation",
            type:"DELETE",
            data:{
                f_account:$("#user").val(),
                friend_account:applicant_account.value,
            },
            success: function (response) {
                alert(response);
                $("#invitation").remove();
                $("#invitationTitle").remove();
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        })
    }
}

function deleteFriend(friend_account)
{
    if (confirm('確定要刪除此好友嗎?')) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
        $.ajax({
            url:"/friend",
            type:"DELETE",
            data:{
                f_account:$("#user").val(),
                friend_account:friend_account.value,
            },
            success: function (response) {
                alert(response);
                $("#friend_account.value").remove();
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        })
    }
}

</script>

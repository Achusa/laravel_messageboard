<meta name="csrf_token" content="{{ csrf_token() }}" />
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    
    <div class="container" align ="center">
        <div class="alert alert-danger" role="alert">
            <font face="微軟正黑體">
                <h1 class="display-4">歡迎來到MessageBoard</h1>
            </font>
        </div>   
    </div>   

</head>

<body style="background-color:#FFF0F5;padding:10px">

<div class="alert alert-warning" role="alert">
    <font face="微軟正黑體">
        <h2 class="display-10">歡迎回來 {{ $user['username'] }}</h2>
    </font>
</div>   
<hr/>

<form action="/friend">
    <button type="submit"  class="btn btn-outline-primary">我的好友名單</button> 
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>

<form method = "post" action = "/">
    <button type="button" id="logout" class="btn btn-outline-danger" onclick="deleteCookie()">登出</button>
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form><hr/>

<form method = "post" action = "/message">
    
    <font face="微軟正黑體">
        <h5>標題</h5>
    </font>
        <input type ="text" name = "title" placeholder="請輸入標題" required>
    <br/>
    <font face="微軟正黑體">
        <h5>留言內容</h5>
    </font>
        <textarea   name = "content" rows="5" cols="40" placeholder="寫下你想說的" required></textarea>
    <br/>
    <font face="微軟正黑體">
        <input type ="submit" class="btn btn-outline-dark"  name = "sendMessage" value="留言">
    </font>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

</form><hr/>

@if (!empty($messageboard))
@foreach ($messageboard as $value)
<form id="message{{ $value['message_id'] }}" action = "/message">
    <tr>
        <td>{{ $value['m_username'] }}({{ $value['m_account'] }})</td>
    </tr>
    <table class="table table-striped w-25 p-3"  style="border-top:3px #FFD382 solid;border-bottom:3px #82FFFF solid;" cellpadding="10" border='0' >
    <input type="hidden" name="id" value ="{{ $value['message_id'] }}">
    <thead>
        <tr>
            <th scope="col">標題</th>
            <th id="title" scope="col">{{ $value['title'] }}</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">留言內容</th>
            <td id="content">{{ $value['content'] }}</td>
        </tr>
        <tr>
            <th scope="row">留言時間</th>
            <td id="updatetime">{{ $value['updatetime']}}</td>
        </tr>
    </tbody>
    </table>
    @if ($user['account'] == $value['m_account'])
    <input type ="text" id = "updateTitle" placeholder="輸入想更新標題"><br/>
    <textarea id = "updateContent" rows="1" cols="23" placeholder="寫下更新的留言" ></textarea><br/>
    <button type="button" class="btn btn-outline-success" value="{{ $value['message_id'] }}" onclick="updateMessage(this)">更新留言</button>
    <button type="button" class="btn btn-outline-danger" value="{{ $value['message_id'] }}" onclick="deleteMessage(this)">刪除</button>
    @endif
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>
@endforeach
@endif

<hr/>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
@if (!empty( $login_message ))
    <input type="hidden" id="message" value =" {{ $login_message}} ">
    <script>
        var msg = $("#message").val();
        alert(msg);
    </script>
@endif
<script>
function deleteMessage(message_id)
{
    if (confirm('確定是否刪除?')) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
        $.ajax({
            url:"/message/"+message_id.value,
            type:"DELETE",
            data: {id:message_id.value},
            success: function () { 
                $("#message"+message_id.value).remove();
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }    
        })
    }

}
function deleteCookie()
{
    if (confirm('確定要登出嗎?')) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
        $.ajax({
            url:"/",
            type:"DELETE",
            success: function () {
                alert('登出成功');(location.href='/');
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        })
    }
}
function updateMessage(message_id)
{
    if (confirm('確定要更新留言嗎?')) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
        $.ajax({
            url:"/message/"+message_id.value,
            type:"PUT",
            data: {id :message_id.value,
                   updateTitle:$("#updateTitle").val(), 
                   updateContent:$("#updateContent").val()
            },
            success: function (response) {
                $("#title").html(response['title']);
                $("#content").html(response['content']);
                $("#updatetime").html(response['updatetime']);
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        })     
    }
}
</script>

</body>
</html>
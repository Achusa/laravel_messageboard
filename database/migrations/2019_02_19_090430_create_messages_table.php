<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_bin';
            $table->increments('message_id')->autoIncrement();
            $table->string('m_username', 24)->index();
            $table->string('m_account', 14)->index();
            $table->string('title', 20);
            $table->text('content');
            $table->timestamp('updatetime')->useCurrent();
            $table->foreign('m_account')->references('account')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('m_username')->references('username')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}

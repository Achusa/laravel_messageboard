<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friend_application', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_bin';
            $table->increments('application_id')->autoIncrement();
            $table->string('applicant_account', 14)->index();
            $table->string('friend_account', 14)->index();         
            $table->foreign('applicant_account')->references('account')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('friend_account')->references('account')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('friend_application');
    }
}

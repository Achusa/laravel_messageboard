<?php

namespace App\Repository;

use Illuminate\Http\Request;
use App\Models\Users;

class UserRepository
{
    public function verifyData($account)
    {
        $result = Users::where('account','=', $account)->get();
        return $result; 
    }

    public function saveUserData($account, $password, $username)
    {
        $secret_password = \Hash::make($password);
        $result = Users::create(['account' => $account, 'password' => $secret_password, 'username' => $username]);
        return $result;
    }

    public function data($account)
    {
        $result = Users::where('account','=', $account)->first();
        $user_data = ['account' => $result['account'], 'username' => $result['username']];
        return $user_data;
    }
}
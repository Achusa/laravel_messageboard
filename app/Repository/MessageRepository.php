<?php

namespace App\Repository;

use Illuminate\Http\Request;
use App\Models\Messages;
use App\Models\Users;

class MessageRepository
{
    public function display($account)
    {
        $result = Messages::all()
            ->sortByDesc('updatetime')
            ->toArray();
        return $result;
    }

    public function user($account)
    {
        $result = Users::where('account','=', $account)->first();
        return $result['username'];
    }
    
    public function insert($account, $username, $title, $content)
    {
        $result = Messages::create([ 'm_account' => $account,
                           'm_username' => $username,
                           'title' => $title,
                           'content' => $content ]);
        return $result;
    }

    public function delete($id)
    {
        Messages::where('message_id', '=', $id)->delete();
    }

    public function updateTitle($id, $title)
    {
        Messages::where('message_id', '=', $id)->update(['title' => $title]);
    }

    public function updateContent($id, $content)
    {
        Messages::where('message_id', '=', $id)->update(['content' =>$content]);
    }
    public function updateData($id)
    {
        $result =  Messages::where('message_id', '=', $id)->first();
        $updateMessage = ['title' => $result['title'], 
                          'content' => $result['content'], 
                          'updatetime' => $result['updatetime']];
        return $updateMessage;
    }
}
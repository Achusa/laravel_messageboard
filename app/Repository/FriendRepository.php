<?php

namespace App\Repository;

use App\Models\FriendApplication;
use App\Models\Friends;

class FriendRepository
{
    public function dispiay($account)
    {
        $result = Friends::all()->Where('f_account', '=', $account)->toArray();
        return $result;
    }
    public function saveSendInvitation($user, $account)
    {
        $result = FriendApplication::create([
            'applicant_account' => $user,
            'friend_account' => $account,
        ]);
        return $result;
    }

    public function verifyInvitation($account, $friend_account)
    {
        $result = FriendApplication::where('applicant_account', '=', $account)
            ->Where('friend_account', '=', $friend_account)
            ->get();
        return $result;
    }

    public function isFriend($account, $friend_account)
    {
        $result = Friends::where('f_account', '=', $account)
            ->Where('friend_account', '=', $friend_account)
            ->get();
        return $result;
    }

    public function invitation($account)
    {
        $result = FriendApplication::where('friend_account', '=', $account)->get();
        return $result;
    }

    public function insert($user, $friend)
    {
        $result = Friends::create([
            'f_account' => $user,
            'friend_account' => $friend,
        ]);
        return $result;
    }

    public function deleteInvitation($user, $friend)
    {
        $result = FriendApplication::where('applicant_account', '=', $friend)
            ->Where('friend_account', '=', $user)
            ->delete();
        return $result;
    }
    
    public function deleteFriend($user, $friend)
    {
        $result = Friends::where('f_account', '=', $user)
            ->Where('friend_account', '=', $friend)
            ->delete();
        return $result;
    }

}

<?php

namespace App\Repository;

use Illuminate\Http\Request;
use App\Models\Users;

class LoginRepository
{
    public function verifyUser($account, $password)
    {
        $db_password = Users::where('account', '=', $account)->first();
        $secret_password = $db_password['password'];
        $result = \Hash::check($password, $secret_password);
        return $result;
    }

    public function checkCookie($cookie)
    {
        $result = Users::where('account', '=', $cookie)->get();
        return $result;
    }
}
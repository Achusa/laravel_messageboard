<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FriendApplication extends Model
{
    /**
     * 與模型關聯的資料表。
     *
     * @var string
     */
    protected $table = 'friend_application';
    protected $primaryKey = 'application_id';
    public $timestamps = false;
    protected $fillable = ['applicant_account', 'friend_account', 'check'];
}
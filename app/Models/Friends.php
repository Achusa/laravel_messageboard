<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Friends extends Model
{
    /**
     * 與模型關聯的資料表。
     *
     * @var string
     */
    protected $table = 'friends';
    protected $primaryKey = 'friend_id';
    public $timestamps = false;
    protected $fillable = ['f_account', 'friend_account'];
}
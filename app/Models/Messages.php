<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    /**
     * 與模型關聯的資料表。
     *
     * @var string
     */
    protected $table = 'messages';
    protected $primaryKey = 'message_id';
    public $timestamps = false;
    protected $fillable = ['message_id','m_account', 'm_username', 'title', 'content'];
}
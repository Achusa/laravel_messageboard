<?php

namespace App\Http\Middleware;

use Closure;

class Verify_login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = \Validator::make($request->all(), [
            'account' => 'required|exists:users|regex:/^(?!.*[^a-zA-Z0-9]).{7,14}$/',
            'password' => 'required|regex:/^(?!.*[^a-zA-Z0-9]).{6,10}$/',
        ]);
        
        if (!$role->passes()) {
            $request['msg'] = '帳號密碼格式錯誤!';
        }
        return $next($request);
    }
}

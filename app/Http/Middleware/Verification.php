<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Validation\Rule;

class Verification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = \Validator::make($request->all(), [
            'username' => 'required|between:3,25',
            'account' => 'required|unique:users|regex:/^(?!.*[^a-zA-Z0-9]).{7,14}$/',
            'password' => 'required|confirmed|regex:/^(?!.*[^a-zA-Z0-9]).{6,10}$/',
        ]);
        //驗證OK則將請求繼續傳送下去
        if (!$role->passes()) {
            $request['msg'] = '申請資料有誤，請重新輸入!';
        }
        return $next($request);
    }
}

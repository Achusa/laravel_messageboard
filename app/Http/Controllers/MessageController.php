<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\MessageRepository;
use App\Services\MessageService;
use Illuminate\Routing\Redirector;

class MessageController extends Controller
{
    public function insert(Request $request)
    {
        $account = \Cookie::get('account');
        $username = (new MessageRepository)->user($account);
        $title = htmlentities($request->title,ENT_QUOTES,"UTF-8");
        $content = htmlentities($request->content,ENT_QUOTES,"UTF-8");
        
        $result = (new MessageRepository)->insert($account, $username, $title, $content);
        //判斷是否成功
        if ($result) {
            return redirect()->action('UserController@index');
        }
    }

    public function delete(Request $request, $id)
    {
        (new MessageRepository)->delete($id);
    }

    public function update(Request $request, $id)
    {
        $title = htmlentities($request->updateTitle,ENT_QUOTES,"UTF-8");
        $content = htmlentities($request->updateContent,ENT_QUOTES,"UTF-8");
        $result = (new MessageService)->update($id, $title, $content);
        return $result;
    }
}
<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use App\Services\LoginService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Repository\FriendRepository;

class UserController extends Controller
{
    /**
     * 顯示應用程式的所有使用者列表。
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $login_result = $request->msg;
        $cookie = \Cookie::get('account');
        $cookie_result = (new UserService)->check($cookie);
        $message = (new MessageRepository)->display($cookie);
        $data = (new UserRepository)->data($cookie);
        
        if (!$cookie_result->isEmpty()) {
            return view('MessageView')
                ->with('messageboard', $message)
                ->with('user', $data)
                ->with('login_message', $login_result);
        } else {
            return view('LoginView');
        }
    }

    public function login(Request $request)
    {
        if (!empty($request['msg'])) {
            $result = $request['msg'];
            return view('loginView')->with('login_message', $result);
        } else {
            $result = (new LoginService)->login($request->account, $request->password);
            if ($result['verify']) {
                return redirect()->route('index', $result);
            } else {
                return view('loginView')->with('login_message', $result['msg']);
            }
        }  
    }

    public function logout()
    {
        (new LoginService)->logout();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    /**
     * 顯示應用程式的所有使用者列表。
     *
     * @return Response
     */
    public function index($request)
    {
        $user = 'aaaaabbbccd'.$request;
        return view('test', ['user' => $user]);
    }
}

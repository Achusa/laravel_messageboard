<?php

namespace App\Http\Controllers;

use App\Repository\FriendRepository;
use App\Repository\UserRepository;
use App\Services\FriendService;
use App\Services\UserService;
use Illuminate\Http\Request;

class FriendController extends Controller
{
    public function index(Request $request)
    {
        $repository = new FriendRepository();
        $service = new FriendService();
        $user = \Cookie::get('account');
        $account = $request->user_account;
        $user_data = (new UserRepository)->data($user);
        $data = $repository->dispiay($user);
        $invitation = $repository->invitation($user);
        //是否有搜尋朋友資料
        if (!empty($account)) {
            $search = $service->search($user, $account);
            $search_result = $search['result'];
            $search_result_msg = $search['msg'];
            return $service->hasSearch($user, $user_data, $invitation, $data, $search_result, $search_result_msg);
        } else {
            return $service->view($user, $user_data, $invitation, $data);
        }
    }

    public function sendInvitation(Request $request)
    {
        $user = $request->applicant_account;
        $account = $request->friend_account;
        
        $result = (new FriendService)->saveSendInvitation($user, $account);
        return $result;
    }

    public function insert(Request $request)
    {
        $user = $request->f_account;
        $account = $request->friend_account;
        
        $result = (new FriendService)->insert($user, $account);
        return $result;
    }
    
    public function deleteInvitation(Request $request)
    {
        $user = $request->f_account;
        $friend = $request->friend_account;
        
        $delete_result = (new FriendRepository)->deleteInvitation($user, $friend);
        
        if ($delete_result) {
            return $result = '已經拒絕此好友邀請';
        }
    }
    public function delete(Request $request)
    {
        $user = $request->f_account;
        $friend = $request->friend_account;
        
        $delete_result = (new FriendRepository)->deleteFriend($user, $friend);
        
        if ($delete_result) {
            return $result = '刪除成功';
        }
    }
}

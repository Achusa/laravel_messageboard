<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;

class RegisterController extends Controller
{
    public function index()
    {
        return view('RegisterView');
    }

    public function saveUserData(Request $request)
    {
        if (!empty($request['msg'])) {
            //錯誤訊息傳到View
            $result = ['msg' => $request['msg'], 'local' => '/register/views'];
            return view('RegisterView')->with('registered_result', $result);
        } else {
            $username = $request->username;
            $account = $request->account;
            $password = $request->password;
            //執行註冊動作
            $result = (new UserService) -> registered($username, $account, $password);
            return view('RegisterView')->with('registered_result', $result);
        }
    }
}

<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Repository\UserRepository;
use App\Services\DataConfirmService;
use App\Repository\LoginRepository;

class UserService
{
    public function check($cookie)
    {
        $result = (new LoginRepository)->checkCookie($cookie);
        return $result;
    }
    
    public function registered($username, $account, $password)
    {
        $result = (new UserRepository)->saveUserData($account, $password, $username);
        if ($result) {
            $result_msg = ['msg' => '註冊成功', 'local' => '/'];
            return $result_msg;
        }
    }
}
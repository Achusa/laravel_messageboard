<?php

namespace App\Services;

use App\Repository\FriendRepository;
use App\Repository\UserRepository;

class FriendService
{
    public function search($user, $account)
    {
        $user_exist = (new UserRepository)->verifyData($account);
        //判別是否有這個人
        if (!$user_exist->isEmpty()) {
            //不能找自己
            if ($user == $account) {
                $result = ['result' => false, 'msg' => '不要找自己麻煩!'];
            } else {
                $result = $this->isFriend($user, $account);
            }
        } else {
            $result = ['result' => false, 'msg' => '查無此使用者，請重新查詢!'];
        }
        return $result;
    }

    public function isFriend($user, $friend)
    {
        $searchuser = (new UserRepository)->data($friend);
        $verify_friend = (new FriendRepository)->isFriend($user, $friend);
        if (!$verify_friend->isEmpty()) {
            $result = ['result' => false, 'msg' => $searchuser];
        } else {
            $result = ['result' => true, 'msg' => $searchuser];
        }
        return $result;
    }

    public function saveSendInvitation($user, $account)
    {
        $verify_result = (new FriendRepository)->verifyInvitation($user, $account);
        if (!$verify_result->isEmpty()) {
            return $result = '你送過好友邀請了!';
        } else {
            $add = (new FriendRepository)->saveSendInvitation($user, $account);
            if ($add) {
                return $result = '送出邀請成功，等待對方接受~~';
            }
        }
    }

    public function insert($user, $friend)
    {
        $repository = new FriendRepository();
        $user_exist = $repository->isFriend($user, $friend);
        $friend_exist = $repository->isFriend($friend, $user);
        if ($user_exist->isEmpty()) {
            $user_restult = $repository->insert($user, $friend);
        }
        if ($friend_exist->isEmpty()) {
            $friend_restult = $repository->insert($friend, $user);
        }
        if ($user_restult && $friend_restult) {
            $repository->deleteInvitation($user, $friend);
            return $result = '你們成為好友囉!';
        }
    }

    public function hasSearch($user, $user_data, $invitation, $data, $search_result, $search_result_msg)
    {
        $cookie_result = (new UserService)->check($user);
        if (!$cookie_result->isEmpty()) {
            return View('FriendView')
                ->with('user', $user_data)
                ->with('invitation', $invitation)
                ->with('friend_data', $data)
                ->with('search_result', $search_result)
                ->with('search_result_msg', $search_result_msg);
        } else {
            return view('LoginView');
        }  
    }

    public function view($user, $user_data, $invitation, $data)
    {
        $cookie_result = (new UserService)->check($user);
        if (!$cookie_result->isEmpty()) {
            return View('FriendView')
                ->with('user', $user_data)
                ->with('invitation', $invitation)
                ->with('friend_data', $data);
        } else {
            return view('LoginView');
        }
    }
}

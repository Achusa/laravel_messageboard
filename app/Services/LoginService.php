<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Repository\LoginRepository;
use App\Services\DataConfirmService;

class LoginService
{
    public function login($account, $password)
    {
        $result = (new LoginRepository)->verifyUser($account, $password);
        if ($result) {
            \Cookie::queue(\Cookie::make('account', $account, 60));
            $message = '登入成功，快來使用!';
        } else {
            $message = '無此帳號，請重新輸入!';
        }
        $login_result = ['verify' => $result, 'msg' => $message];
        return $login_result;
    }

    public function logout()
    {
        \Cookie::queue(\Cookie::forget('account'));
    }
}
<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Repository\MessageRepository;


class MessageService
{
    public function update($id, $title, $content)
    {
        $message_obj = new MessageRepository();
        if (!empty($title)) {
            $message_obj->updateTitle($id, $title);
        }
        if (!empty($content)) {
            $message_obj->updateContent($id, $content);
        }
        return $message_obj->updateData($id);
    }
}
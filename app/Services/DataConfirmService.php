<?php

namespace App\Services;

use Illuminate\Http\Request;

class DataConfirmService
{
    public function confirmUsername($username) //確認註冊資料輸入是否有誤
    {
        if (empty($username)) { 
            return false;
        }
        if (strlen($username)<=3 or strlen($username)>=25) {  
            return false;
        }
        return true;
    }

    public function confirmAccount($account) //確認註冊資料輸入是否有誤
    {    
        if (empty($account)) { 
            return false;
        }
        if (strlen($account)<=6 or strlen($account)>=15) {  
            return false;
        }
        $pattern_account = "/^(?!.*[^a-zA-Z0-9]).{7,14}$/";
        if (preg_match_all($pattern_account, $account)) {
            return true;
        }
    }    
    
    public function confirmPassword($password) //確認註冊資料輸入是否有誤
    {    
        if (empty($password)) { 
            return false;
        }
        if (strlen($password)<=5 or strlen($password)>=15) {  
            return false;;
        }
        $pattern_password = "/^(?!.*[^a-zA-Z0-9]).{6,10}$/";
        if (preg_match_all($pattern_password, $password)) {
            return true;
        }
    }

    public function confirmCheckPassword($password, $confirm_password) //確認註冊資料輸入是否有誤
    {
        if (empty($confirm_password)) { 
            return false;
        }
        if (strlen($confirm_password)<=5 or strlen($confirm_password)>=15) {  
            return false;
        }
        if ($password != $confirm_password) {
            return false;
        }
        return true;
    }
}
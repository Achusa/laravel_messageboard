<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('test')->get('/test/{id}', 'TestController@index');

Route::get('/', 'UserController@index')->name('index');
Route::get('/register/views', 'RegisterController@index');
Route::middleware('verification')->post('/register/data', 'RegisterController@saveUserData');
Route::middleware('verify_login')->post('/', 'UserController@login');
Route::delete('/', 'UserController@logout');

Route::post('/message', 'MessageController@insert');
Route::delete('/message/{id}', 'MessageController@delete')->where('id', '[0-9]+');
Route::put('/message/{id}', 'MessageController@update')->where('id', '[0-9]+');

Route::get('/friend', 'FriendController@index');
Route::post('/friend', 'FriendController@index');
Route::put('/invitation', 'FriendController@sendInvitation');
Route::delete('/invitation', 'FriendController@deleteInvitation');
Route::put('/friend', 'FriendController@insert');
Route::delete('/friend', 'FriendController@delete');